﻿using System.Collections.Generic;
using UnityEngine;

namespace Implementation
{
    /// <summary>
    /// Unweighted oriented graph class
    /// </summary>
    public class OrientedGraph : MonoBehaviour
    {
        #region Variables

        #endregion

        #region Properties

        /// <summary> Number of vertices</summary>
        public int VertNum => Vertices.Count;
    
        public static OrientedGraph Main => FindObjectOfType<OrientedGraph>();

        #endregion

        #region Fields

        /// <summary> Adjacency lsit </summary>
        public List<int[]> Edges;

        /// <summary> List of nodes </summary>
        public List<BasePanel> Vertices;

        #endregion

        #region Methods

        private void Start()
        {
            Edges = new List<int[]>();
            for (int i = 0; i < VertNum; i++)
            {
                Edges.Add(null);
            }
        }

        /// <summary>
        /// Breadth-First Search
        /// </summary>
        /// <list type="http://e-maxx.ru/algo/bfs">Link</list>
        /// <param name="from">Source</param>
        /// <param name="to">Target destination</param>
        /// <returns>Returns path</returns>
        public int[] BFS_FindPath(int from, int to)
        {
            var q = new Queue<int>();
            q.Enqueue(from);
            // Visited vertices
            var used = new bool[VertNum];
            used[from] = true;
            // Array of paths' length
            var d = new int[VertNum];
            // Array of 'ancestors'
            var p = new int[VertNum];
            p[from] = -1;
            while (q.Count > 0)
            {
                var v = q.Dequeue();
                foreach (var i in Edges[v])
                {
                    var next = i;
                    if (!used[next])
                    {
                        used[next] = true;
                        q.Enqueue(next);
                        d[next] = d[v] + 1;
                        p[next] = v;
                        if (next == to)
                        {
                            q.Clear();
                            break;
                        }
                    }
                }
            }

            var path = new List<int>();
            if (!used[to])
            {
                return path.ToArray();
            }
            else
            {
                for (var i = to; i != -1; i = p[i])
                {
                    path.Add(i);
                }

                path.Reverse();
                return path.ToArray();
            }
        }

        public int PathLength(int from, int to) => BFS_FindPath(from, to).Length;
    

        public int[] BFS_FindInRange(int from, int range)
        {
            var q = new Queue<int>();
            q.Enqueue(from);
            // Visited vertices
            var lit = new List<int>();
            lit.Add(from);
            // Array of paths' length
            var d = new int[VertNum];
            while (q.Count > 0)
            {
                var v = q.Dequeue();
                foreach (var i in Edges[v])
                {
                    var next = i;
                    if (!lit.Contains(next))
                    {
                        if(d[v] + 1 > range)
                            break;
                        lit.Add(next);
                        q.Enqueue(next);
                        d[next] = d[v] + 1;
                    }
                }
            }

            return lit.ToArray();
        }

        public void UpdateIds()
        {
            foreach (var basePanel in Vertices)
            {
                basePanel.Id = Vertices.IndexOf(basePanel);
            }
        }

        #endregion
    }
}