﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Implementation
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(BasePanel))]
    public class NodeSetup : MonoBehaviour
    {
        #region Variables

        #endregion

        #region Fields

        public List<NodeSetup> ConnectedNodes;
        public BasePanel Panel;
        public OrientedGraph Graph;

        #endregion

        private void Awake()
        {
            if (Application.isPlaying) return;

            Panel = GetComponent<BasePanel>();
            Graph = OrientedGraph.Main;
            name = "Panel (" + Panel.Id.ToString() + ")";
            if (Graph.Vertices.Contains(Panel)) return;
            Panel.Id = Graph.VertNum;
            Graph?.Vertices.Add(Panel);
        }

        private void Start()
        {
            if (!Application.isPlaying) return;
            var tempArray = new List<int>();

            foreach (var node in ConnectedNodes)
            {
                tempArray.Add(node.Panel.Id);
            }

            Graph.Edges[Panel.Id] = tempArray.ToArray();
        }

        private void OnDestroy()
        {
            if (Application.isPlaying) return;
            Graph?.Vertices.Remove(Panel);
            Graph?.UpdateIds();
        }

        private void OnDrawGizmos()
        {
            GUIStyle guiStyle = new GUIStyle();
            guiStyle.normal.textColor = Color.red;
            guiStyle.fontStyle = FontStyle.Normal;
            guiStyle.fontSize = 16;

            Handles.Label(transform.position, Panel.Id.ToString(), guiStyle);

            Handles.color = Color.blue;

            if (ConnectedNodes == null) return;

            foreach (var panel in ConnectedNodes)
            {
                if (panel == null || panel == Panel) continue;
                var direction = (panel.transform.position - transform.position).normalized;
                Handles.ArrowHandleCap(0, transform.position, Quaternion.LookRotation(direction), 0.5f, EventType.Repaint);
            }
        }
    }
}