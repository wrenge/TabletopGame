﻿using System;
using UnityEngine;

namespace Implementation
{
    public abstract class BasePanel : MonoBehaviour
    {
        #region Constants

        #endregion
        
        #region Variables

        private OrientedGraph _graph;

        /// <summary> Default color of the panel </summary>
        private Color _defColor;

        #endregion

        #region Fields

        /// <summary> Id in graph </summary>
        public int Id;

        #endregion

        #region Properties

        #endregion

        #region Events

        public event EventHandler<StepEventArgs> StepEvent;
        public event EventHandler<StepEventArgs> StandEvent;

        #endregion

        #region Methods

        protected virtual void Start()
        {
            _defColor = GetComponent<Renderer>().material.color;
        }

        public virtual void OnStep(StepEventArgs e)
        {
            var handler = StepEvent;
            handler?.Invoke(this, e);
        }

        public virtual void OnStand(StepEventArgs e)
        {
            var handler = StandEvent;
            handler?.Invoke(this, e);
        }

        public virtual void Highlight()
        {
            // TODO: Improve highlighting
            GetComponent<Renderer>().material.color = Color.red;
        }

        public virtual void UnHighlight()
        {
            GetComponent<Renderer>().material.color = _defColor;
        }

        #endregion
    }

    public class StepEventArgs : EventArgs
    {
        public BaseCharacter Activator { get; set; }
    }
}