﻿using System;
using UnityEngine;

namespace Implementation
{
    public class BaseCharacter : MonoBehaviour
    {
        #region Constants

        private const int MaxMana = 6;
        private const float PlayerSpeed = 1;

        private const float WaypointDist = 0.1f;

        #endregion

        #region Variables

        private int[] _path;
        private int _moveCounter;

        #endregion

        #region Properties

        public short Health { get; private set; }
        public short Mana { get; set; } = MaxMana;
        public int CurrentPanelId { get; set; }
        public PlayerMind Controller { get; private set; }

        #endregion

        #region Events

        public event EventHandler<EventArgs> TurnStart;
        public event EventHandler<EventArgs> TurnEnd;

        #endregion

        #region Methods

        private void Start()
        {
            transform.position = OrientedGraph.Main.Vertices[CurrentPanelId].gameObject.transform.position;
        }

        private void FixedUpdate()
        {
            if (_path != null) ExecuteMovement();
        }

        protected virtual void ExecuteMovement()
        {
            if (_path == null) return;
            var targetPos = OrientedGraph.Main.Vertices[CurrentPanelId].gameObject.transform.position;
            if ((targetPos - transform.position).magnitude <= WaypointDist)
            {
                transform.position += (targetPos - transform.position).normalized * PlayerSpeed * Time.fixedDeltaTime;
            }
            else
            {
                if (_moveCounter >= _path.Length)
                {
                    Stand(_path[_moveCounter]);
                    _path = null;
                    _moveCounter = 0;
                    return;
                }

                Step(_path[_moveCounter]);
                _moveCounter++;
            }
        }

        protected virtual void OnRoundStart(EventArgs e)
        {
            Mana = MaxMana;
        }

        public virtual void OnTurnStart(EventArgs e)
        {
            Controller.OnTurnStart();
            var handler = TurnStart;
            handler?.Invoke(this, e);
        }

        public virtual void OnTurnEnd(EventArgs e)
        {
            Controller.OnTurnEnd();
            var handler = TurnEnd;
            handler?.Invoke(this, e);
        }

        protected virtual void Step(int nodeId)
        {
            var panel = OrientedGraph.Main.Vertices[nodeId];
            var args = new StepEventArgs {Activator = this};
            panel.OnStep(args);
        }

        protected virtual void Stand(int nodeId)
        {
            CurrentPanelId = nodeId;

            var panel = OrientedGraph.Main.Vertices[nodeId];
            var args = new StepEventArgs {Activator = this};
            panel.OnStand(args);

            if (Controller == null) throw new Exception("Character's moving w/o controller");
            Controller.OnStand();
        }

        public virtual void GoToNode(int targetNode)
        {
            GoOnPath(OrientedGraph.Main.BFS_FindPath(CurrentPanelId, targetNode));
        }

        public virtual void GoOnPath(int[] path)
        {
            _path = path;
            Mana -= (short) _path.Length;
        }

        public virtual void EndTurn()
        {
            MatchState.Main.OnEndTurn(EventArgs.Empty);
        }

        public virtual void OnPossess(PlayerMind mind)
        {
            Controller = mind;
        }

        public virtual void OnUnPossess()
        {
            Controller = null;
        }

        #endregion
    }

    public enum CharacterState
    {
        Idle,
        Move,
        Fight,
        Dead
    }
}